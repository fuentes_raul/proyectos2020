<%-- 
    Document   : salida
    Created on : 29-03-2020, 20:32:00
    Author     : ANDRES
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Resultado</h1>
        
        <%
            String nombre = (String) request.getAttribute("nombre");
            String seccion = (String) request.getAttribute("seccion");
        %>
        
        <p>Hola  <%= nombre%>, tu seccion es : <%=seccion%></p>
        
        
    </body>
</html>
